import React from 'react';
import './App.scss';
import Button from './components/button';
import Modal from './components/mod';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      visModalOne: false,
      visModalTwo: false,
    }

    this.btnClickOne = this.btnClickOne.bind(this);
    this.btnClickCloseOne = this.btnClickCloseOne.bind(this);
    this.btnClickTwo = this.btnClickTwo.bind(this);
    this.btnClickCloseTwo = this.btnClickCloseTwo.bind(this);
    
  }
  render(){
    return(
      <>
      <Button onOpenClick={this.state.visModalOne ? this.btnClickCloseOne : this.btnClickOne} btnTxt="Modal one" />
      <Button onOpenClick={this.state.visModalTwo ? this.btnClickCloseTwo : this.btnClickTwo} btnTxt="Modal two" />
        <>
        {this.state.visModalOne ? <Modal title ="Modal title One!" text = "1111"  cls = {this.btnClickCloseOne}/> : ""}
        </>
        <>
        {this.state.visModalTwo ? <Modal title ="Modal title Two!" text = "2222" cls = {this.btnClickCloseTwo}/> : ""}
        </>
      </>
    )
  }

  btnClickOne(){
    this.setState({visModalOne: true})
    console.log("click")
    console.log(this.state.visModalOne)
  }

  btnClickCloseOne(){
    this.setState({visModalOne: false})
    console.log("clickClose")
    console.log(this.state.visModalOne)
  }

  btnClickTwo(){
    this.setState({visModalTwo: true})
    console.log("click222")
  }

  btnClickCloseTwo(){
    this.setState({visModalTwo: false})
    console.log("clickClose")
    console.log(this.state.visModalTwo)
  }

}

export default App;
