import React from 'react';
import ButtonMod from './buttonMod';

class Modal extends React.Component {
    constructor(props){
        super(props)
        
        this.state = {
            closeButton: true,
        }


    }

    render() {
        return (
            <div className='modal'>
            <div className='modOver' onClick={this.props.cls}></div>
            <div className='modWind'>
                <div className='modHeader'>
                    <div className='headerTitle'>{this.props.title}</div>
                    <div className='headerClose' onClick={this.props.cls} style={{display: this.state.closeButton === true ? "block" : "none" }} >X</div>
                </div>
                <div className='modText'>{this.props.text}</div>
                <div className='modFooter'>
                    <ButtonMod onCloseClick={this.props.cls} btnTxt="Cancel"/>
                    <ButtonMod onClick={this.props.onSubmit} btnTxt="Submit" btnBgCol="#696969" btnCol="#FFFFFF"/>
                </div>
            </div>
            </div>
            
            
        )
    }
}
    
    Modal.defaultProps = {
        title: "Modal title!",
        text: "22222222222",
        onSubmit: () => {},
    }

export default Modal