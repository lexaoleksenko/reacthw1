import React from 'react';

class Button extends React.Component {
    render() {
        return (
            <button onClick={this.props.onOpenClick} className="btn">{this.props.btnTxt}</button>
        )
    }

}

export default Button